package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"os"
)

const PORT = 14990

type Message struct {
	Channel string // reserved
	From    string
	FromIP  net.IP
	Text    string
}

type Chat struct {
	name string
	conn net.Conn

	R <-chan Message
}

func (c *Chat) Send(text string) error {
	m := Message{
		From: c.name,
		Text: text,
	}

	data, err := json.Marshal(m)
	if err != nil {
		return fmt.Errorf("Marshal message: %v", err)
	}

	_, err = c.conn.Write(data)
	if err != nil {
		return fmt.Errorf("Send message to chat: %v", err)
	}

	return nil
}

func (c *Chat) Name(name string) {
	c.name = name
}

func NewChat(conn net.Conn) *Chat {

	r := make(chan Message, 100)
	go func(c chan Message) {
		socket, err := net.ListenUDP("udp", &net.UDPAddr{
			IP:   net.IPv4(0, 0, 0, 0),
			Port: PORT,
		})
		if err != nil {
			log.Fatal("Listen UDP:", err)
		}

		for {
			data := make([]byte, 4096)
			n, remote, err := socket.ReadFromUDP(data)
			if err != nil {
				log.Printf("Read from socket: %v", err)
				continue
			}

			m := Message{}
			err = json.Unmarshal(data[:n], &m)
			if err != nil {
				log.Printf("Unmarshal recived message: %v", err)
				continue
			}

			m.FromIP = remote.IP
			r <- m
		}
	}(r)

	return &Chat{
		conn: conn,
		R:    r,
	}
}

func main() {
	ip := net.IPv4(255, 255, 255, 255)
	sock, err := net.DialUDP("udp", nil, &net.UDPAddr{
		IP:   ip,
		Port: PORT,
	})

	if err != nil {
		log.Fatal("Dial UDP:", err)
	}

	chat := NewChat(sock)

	var name string
	fmt.Print("Your name: ")
	fmt.Scanln(&name)
	chat.Name(name)

	go func(c *Chat) {
		for m := range c.R {
			fmt.Printf("[%s (%s)] %s\n", m.From, m.FromIP.String(), m.Text)
		}
	}(chat)

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		text := scanner.Text()
		chat.Send(text)

		//fmt.Printf(">>> [%s (0.0.0.)] %s\n", name, text)
	}
}
